<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<?php   

function Fibonacci($number){ 
      
    if ($number == 0) 
        return 0;     
    else if ($number == 1) 
        return 1;     
      
    else
        return (Fibonacci($number-1) +  
                Fibonacci($number-2)); 
} 
  
$number = 100; 
for ($counter = 0; $counter < $number; $counter++){   
    echo Fibonacci($counter),' '; 
} 
?>
</body>
</html>